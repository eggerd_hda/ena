package h_da.dueckh_demuess.sep;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


// Startseite der App die ein paar Statistiken ausgibt und die Möglichkeit gibt zur Übersicht
// aller bestellter Tools zu wächseln, oder zu einer neuen Bestellung zu wechseln.
public class main extends ActionBarActivity
{
    // beim Aufruf der Activity
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // View auf die aktuelle Activity setzen

        fetch_overview(); // Daten für die Overview laden und ausgeben
    }


    // wertet die in "orders.txt" gespeicherten Daten aus um eine Art Statistik zu erstellen,
    // die Auskunft über die Anzahl der Geräte und die damit verbundenen Kosten berechnet.
    public void fetch_overview()
    {
        List<String> orders = new ArrayList<String>(); // speichert je eine Zeile aus orders.txt
        HashMap<String, List<String>> tools = new HashMap<String, List<String>>(); // ordnet jeder Kategorie (Key) eine Liste von Tools zu (= eine Zeile aus tools.txt)

        try
        {
            InputStream input;
            BufferedReader reader;
            String line;

            input = openFileInput("orders.txt"); // Datei im InternalStorage öffnen
            reader = new BufferedReader(new InputStreamReader(input));

            while ((line = reader.readLine()) != null) // bis keine Zeile zum lesen übrig ist
            {
                orders.add(line); // Zeile (Order) in der Liste speichern (z.B. 1;3;0 => Kategorie;Gerät;Projekt)
            }

            try
            {
                int cat_id_counter = -1; // zählt bei welcher Kategorie sich die Schleife nun befindet

                input = getAssets().open("tools.txt"); // Datei aus den Assets öffnen (ReadOnly)
                reader = new BufferedReader(new InputStreamReader(input));

                List<String> tool_data = new ArrayList<String>(); // Zwischenspeicher für alle Tools der aktuellen Kategorie

                while((line = reader.readLine()) != null) // solange bis keine weitere Zeile mehr vorhanden ist
                {
                    String[] data = line.split(";"); // Daten der aktuellen Zeile auftrennen

                    if(data.length > 0) // nur wenn beim Split erfolgreich war
                    {
                        if(data[0].equals("===")) // falls es sich um eine Kategorie handelt
                        {
                            if(tool_data.size() > 0) // sollten im Zwischenspeicher noch Daten zum speichern sein
                            {
                                tools.put(new Integer(cat_id_counter).toString(), new ArrayList<String>(tool_data)); // Kopie der Tools-Daten unter Kategorie-ID abspeichern
                            }

                            cat_id_counter++;
                            tool_data.clear(); // Zwischenspeicher leeren, da neue Kategorie erreicht wurde
                        }
                        else
                        {
                            tool_data.add(line); // Tool-Daten dem Zwischenspeicher hinzufügen
                        }
                    }
                }

                tools.put(new Integer(cat_id_counter).toString(), new ArrayList<String>(tool_data)); // Kopie der Tools-Daten unter Kategorie-ID abspeichern


                // Verbindungsobjekte zu allen benötigten Textelementen erstellen
                TextView overview_amount = (TextView) findViewById(R.id.overview_amount);
                TextView overview_costs_hour = (TextView) findViewById(R.id.overview_costs_hour);
                TextView overview_costs_day = (TextView) findViewById(R.id.overview_costs_day);
                TextView overview_costs_week = (TextView) findViewById(R.id.overview_costs_week);
                TextView overview_costs_month = (TextView) findViewById(R.id.overview_costs_month);

                overview_amount.setText(new Integer(orders.size()).toString() + " Stück"); // Anzahl der bestellten Geräte ausgeben

                double cost_hour = 0, cost_day = 0, cost_week = 0, cost_month = 0; // zum Aufaddieren der Kosten

                for(int i = 0; i < orders.size(); i++) // für jede Bestellung
                {
                    String[] order = orders.get(i).split(";")[0].split(":"); // Bestell-Daten auftrennen (z.B. 1:3;0 -> Kategorie:Gerät;Projekt)
                    String[] tool = tools.get(order[0]).get(Integer.parseInt(order[1])).split(";"); // Daten des zugehörigen Tools auftrennen (z.B. xyz;1.99;d;lorem -> Name;Preis;Dauer[h/d/w/m];Beschreibung)

                    switch (tool[2]) // anhand der Preis-Dauer switchen
                    {
                        // Kosten für das aktuelle Tool auf die jeweilige Variable aufaddieren
                        case "h":
                            cost_hour += Double.parseDouble(tool[1]);
                            break;
                        case "d":
                            cost_day += Double.parseDouble(tool[1]);
                            break;
                        case "w":
                            cost_week += Double.parseDouble(tool[1]);
                            break;
                        case "m":
                            cost_month += Double.parseDouble(tool[1]);
                            break;
                    }
                }

                // die Kosten der kleineren Dauer den Kosten der höheren Dauer hinzufügen
                cost_day += cost_hour * 24;
                cost_week += cost_day * 7;
                cost_month += cost_day * 31;

                // errechnete Kosten ausgeben
                overview_costs_hour.setText(new DecimalFormat("#.##").format(cost_hour) + " €");
                overview_costs_day.setText(new DecimalFormat("#.##").format(cost_day) + " €");
                overview_costs_week.setText(new DecimalFormat("#.##").format(cost_week) + " €");
                overview_costs_month.setText(new DecimalFormat("#.##").format(cost_month) + " €");
            }
            catch (Exception e) {}
        }
        catch (Exception e) {}
    }


    // wird bei einem Klick auf den "+"-Button aufgerufen
    public void button_addTool(View view)
    {
        Intent intent = new Intent(this, add_tool.class); // zu öffnende Activity definieren
        startActivity(intent); // definierte Activity starten
    }


    // wird bei einem Klicke auf "Bestellte Geräte" aufgerufen
    public void button_orders(View view)
    {
        Intent intent = new Intent(this, orders.class); // zu öffnende Activity definieren
        startActivity(intent); // definierte Activity starten
    }


    /* Würde in der Navigations-Leiste More->Settings einbauen, was in unserer App aber nicht
     * implementiert wurde..

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
