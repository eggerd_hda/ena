package h_da.dueckh_demuess.sep;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.view.View;
import android.widget.ExpandableListView.OnChildClickListener;


public class orders extends ActionBarActivity
{
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader; // enthält später die Namen aller Kategorien
    HashMap<String, List<String>> listDataChild; // weißt jeder Kategorie (Key) einen Liste von Daten zu

    List<String> orders = new ArrayList<String>(); // Liste aller vorhandenen Bestellungen
    List<String> cats = new ArrayList<String>(); // Liste aller vorhandenen Kategorien
    HashMap<String, List<String>> tools = new HashMap<String, List<String>>(); // Liste aller vorhandenen Tools, gruppiert nach Kategorie (Key)
    HashMap<Integer, HashMap<Integer, String>> data_map = new HashMap<Integer, HashMap<Integer, String>>(); // weißt jedem Eintrag der Listview seinen Order-Datensatz zu (z.B. ListGroup 0, ListChild 2 = 1;3;0 [Kategorie;Tool;Projekt])


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        prepareListData(); // Daten für die ListView zusammenstellen
        expListView = (ExpandableListView) findViewById(R.id.lvExp); // Verbindungsobjekt für die ListView erstellen
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild); // erstelle Daten zur ListView übertragen
        expListView.setAdapter(listAdapter); // der ListView unseren Adapter (mit den Daten) zuweisen

        for(int i = 0; i < expListView.getExpandableListAdapter().getGroupCount(); i++) // für jede vorhandene Gruppe in der ListView
        {
            expListView.expandGroup(i, false); // ohne Animation aufklappen
        }

        // Listview on child click Listener
        expListView.setOnChildClickListener(new OnChildClickListener()
        {
            // löscht das angeklickte Tool aus der Order-Datei
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
            {
                List<String> file = new ArrayList<String>(); // enthält später alle Zeilen der Order-Datei, die dann wieder gespeichert werden sollen

                try
                {
                    InputStream input;
                    BufferedReader reader;
                    String line;
                    boolean purged = false; // Flag, ob unsere Zeile bereits gelöscht wurde

                    input = openFileInput("orders.txt"); // Datei im InternalStorage öffnen
                    reader = new BufferedReader(new InputStreamReader(input));

                    while ((line = reader.readLine()) != null) // solange Zeilen zum Lesen vorhanden sind
                    {
                        // In der Data-Map an Stelle der GruppenID (= Kategorie) und dann ChildID (= Tool) den zugehörigen
                        // Datensatz auslesen (z.B. 1;3;0 [Kategorie;Tool;Projekt]), damit dieser mit der aktuellen Zeile
                        // der Order-Datei verglichen werden kann - somit wird die Verbindung zwischen den ListView IDs und
                        // unseren echten Datensätzen hergestellt.
                        if(line.trim().equals(data_map.get(groupPosition).get(childPosition)) && purged == false )
                        {
                            purged = true; // Datensatz als gelöscht markieren
                            // Löschung erfolgt dadruch, dass die aktuelle Zeile nicht in der file-Liste abgelegt wird, die
                            // später wieder komplett in die Datei geschrieben wird.
                        }
                        else // es handelt sich nicht um den gesuchten Datensatz
                        {
                            file.add(line); // Zeile in file-Liste abspeichern, wird später wieder in die Datei geschrieben
                        }
                    }


                    try
                    {
                        OutputStream output;
                        output = openFileOutput("orders.txt", Context.MODE_PRIVATE); // Datei zum schreiben öffnen (nicht im Append-Mode, wird also neu angelegt!)

                        for(int i = 0; i < file.size(); i++) // für jede gespeicherte Zeile in der file-Liste
                        {
                            output.write((file.get(i) + "\n").getBytes()); // die aktuelle Zeile wieder in die Datei schreiben
                        }

                        output.close(); // Datei wieder schließen
                    }
                    catch (Exception e) {}

                    finish(); // aktuelle Activity beenden
                    startActivity(getIntent()); // Activity wieder neu staten um ListView neu zu erzeugen und somit zu aktualisieren
                }
                catch (Exception e) {}

                return false;
            }
        });
    }


    // bereitet die Daten für das ExpandableListView vor, in dem die Daten geschachtelt werden
    private void prepareListData()
    {
        listDataHeader = new ArrayList<String>(); // Liste aller Header (= Kategorien)
        listDataChild = new HashMap<String, List<String>>(); // Map mit Key (Kategorie) => Liste zugehöriger Daten
        List<String> list_data = new ArrayList<String>(); // Zwischenspeicher für die Daten der aktuellen Kategorie

        try
        {
            InputStream input;
            BufferedReader reader;
            String line;

            input = openFileInput("orders.txt"); // Datei im InternalStorage öffnen
            reader = new BufferedReader(new InputStreamReader(input));

            while ((line = reader.readLine()) != null) // solange Zeilen vorhanden sind
            {
                orders.add(line); // Order-Daten abspeichern
            }


            try
            {
                int cat_id_counter = -1; // Counter = Cat ID

                input = getAssets().open("tools.txt"); // Asset-Datei öffnen
                reader = new BufferedReader(new InputStreamReader(input));

                List<String> tool_data = new ArrayList<String>(); // Zwischenspeicher für alle Tools der aktuellen Kategorie

                while((line = reader.readLine()) != null) // solange Zeilen zum Lesen vorhanden sidn
                {
                    String[] data = line.split(";"); // Tool-Daten auftrennen

                    if(data.length > 0) // nur wenn auch Daten aufgetrennt werden konnten
                    {
                        if(data[0].equals("===")) // falls es sich bei den aktuellen Daten um eine Kategorie handelt
                        {
                            if(tool_data.size() > 0) // falls noch Daten im Zwischenspeicher sind
                            {
                                tools.put(new Integer(cat_id_counter).toString(), new ArrayList<String>(tool_data)); // Kopie der Daten abspeichertn (Kategorie => Daten[])
                                tool_data.clear(); // Zwischenspeicher wieder leeren, da mit neuer Kateogie begonnen wird
                            }

                            cats.add(data[1]); // Name der Kategorie abspeichern
                            cat_id_counter++;
                        }
                        else
                        {
                            tool_data.add(line); // Tool-Daten abspeichern
                        }
                    }
                }

                tools.put(new Integer(cat_id_counter).toString(), new ArrayList<String>(tool_data)); // Kopie der Daten abspeichertn (Kategorie => Daten[])


                HashMap<Integer, String> mapping = new HashMap<Integer, String>();

                for(int i = 0; i < tools.size(); i++) // für jede Kategorie in der Tool-Liste
                {
                    list_data.clear(); // Zwischenspeicher leeren
                    mapping.clear(); // Map leeren
                    String cat = new Integer(i).toString(); // Kategorie ID in String umwandeln

                    for(int n = 0; n < orders.size(); n++) // für jede gespeicherte Bestellung
                    {
                        String[] order = orders.get(n).split(";")[0].split(":"); // Order-Data auftrennen

                        if(order[0].equals(cat)) // wenn die Cat-ID der Bestellung mit der aktuellen Kategorie übereintimmt
                        {
                            String[] line_data = tools.get(cat).get(Integer.parseInt(order[1])).split(";"); // Tool-Daten des zugehörigen Tools auftrennen
                            String dur = null;

                            switch (line_data[2]) // anhand der Price-Dauer switchen
                            {
                                case "h":
                                    dur = "Stunde";
                                    break;
                                case "d":
                                    dur = "Tag";
                                    break;
                                case "w":
                                    dur = "Woche";
                                    break;
                                case "m":
                                    dur = "Monat";
                                    break;
                            }

                            list_data.add(line_data[0].trim() + "\n" + line_data[1] + " € / " + dur); // Tool mit Preis-Info im Zwischenspeicher ablegen
                            mapping.put(mapping.size(), orders.get(n)); // neuen Eintrag an nächstem Index (entspricht später dem Index in der ListView); Order-Daten speichern
                        }
                    }

                    if(list_data.size() > 0) // wenn sich Daten im Zwischenspeicher befinden
                    {
                        // Erklärung: Jeder Datensatz der in der ListView landet wird einer Gruppe zugeordnet. In der Reihenfolge wie
                        // wir diese im Array abspeichern, werden diese einfach durchnummeriert. Ebenso werden jeder Kategorie Tools
                        // zugewiesen, die ebenfalls durchnummeriert werden.
                        // Wird in der ListView das 3. Tool aus der 1. Kategorie angeklickt, erhalten wir in der onClick Funktion die
                        // Daten groupPosition = 1 & childPosition = 3; Um die entsprächende Bestellung aud der Datei löschen zu können,
                        // müssen wir wissen wie der zugehörige Datensatz aussieht, da wir in dieser Liste von der GroupID nicht auf die
                        // wahre GruppenID in der Datei schließen können, da dies je nach Reihenfolge der Datensätze unterschiedlich sein
                        // kann!
                        // Die Map speichert also unter den selben ID's wie die ListView den jeweils zugehörigen Datensatz. Sodass
                        // bei einem onClick der zugehörige Datensatz aus der Map gelesen werden kann und dann mit den Einträgen der
                        // Datei verglichen werden kann.
                        data_map.put(data_map.size(), new HashMap<Integer, String>(mapping)); // Map für diese Kategorie in der Data-Map ablegen

                        listDataHeader.add(cats.get(i)); // Name der Kategorie speichern
                        listDataChild.put(listDataHeader.get(listDataHeader.size() - 1), new ArrayList<String>(list_data)); // Kopie der Daten der Kategorie zuweisen
                    }
                }
            }
            catch (Exception e) {}
        }
        catch (Exception e) {}
    }


    /* Würde in der Navigations-Leiste More->Settings einbauen, was in unserer App aber nicht
     * implementiert wurde..

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_tool, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
