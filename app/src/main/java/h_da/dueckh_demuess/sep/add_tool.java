package h_da.dueckh_demuess.sep;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.view.View;
import android.widget.ExpandableListView.OnChildClickListener;


// zeigt eine Liste, gruppiert nach Kategorie, aller Geräte die der Benutzer bestellen kann
public class add_tool extends ActionBarActivity
{
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader; // enthält später die Namen aller Kategorien
    HashMap<String, List<String>> listDataChild; // weißt jeder Kategorie (Key) einen Liste von Daten zu


    // beim Aufruf der Activity
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tool); // View auf die aktuelle Activity setzen

        prepareListData(); // Daten für die ListView zusammenstellen
        expListView = (ExpandableListView) findViewById(R.id.lvExp); // Verbindungsobjekt für die ListView erstellen
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild); // erstelle Daten zur ListView übertragen
        expListView.setAdapter(listAdapter); // der ListView unseren Adapter (mit den Daten) zuweisen

        // Listview on child click Listener
        expListView.setOnChildClickListener(new OnChildClickListener()
        {
            // öffnet die Detailansicht für das angeklickte Tool
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
            {
                Intent intent = new Intent(v.getContext(), tool_data.class); // Activity definieren, die aufgerufen werden soll
                intent.putExtra("cat_id", groupPosition); // Daten die übergeben werden sollen
                intent.putExtra("tool_id", childPosition); // Daten die übergeben werden sollen
                startActivity(intent); // definierte Activity starten und Daten übergeben

                return false;
            }
        });
    }


    // bereitet die Daten für das ExpandableListView vor, in dem die Daten geschachtelt werden
    private void prepareListData()
    {
        InputStream input;
        BufferedReader reader;
        String line;

        listDataHeader = new ArrayList<String>(); // Liste aller Header (= Kategorien)
        listDataChild = new HashMap<String, List<String>>(); // Map mit Key (Kategorie) => Liste zugehöriger Daten
        List<String> list_data = new ArrayList<String>(); // Zwischenspeicher für die Daten der aktuellen Kategorie

        try
        {
            input = getAssets().open("tools.txt"); // Asset-Datei öffnen
            reader = new BufferedReader(new InputStreamReader(input));

            while((line = reader.readLine()) != null) // solange bis keine Zeile mehr übrig ist
            {
                String[] data = line.split(";"); // Tool-Daten auftrennen

                if(data.length > 0) // nur wenn auch Daten aufgetrennt werden konnten
                {
                    if(data[0].equals("===")) // falls es sich bei den Daten um eine Kategorie handelt
                    {
                        if(list_data.size() > 0 && listDataHeader.size() > 0) // falls noch Daten im Zwischenspeicher sind (und bereits eine Kategorie angelegt wurde) diese abspeichern
                        {
                            listDataChild.put(listDataHeader.get(listDataHeader.size() - 1), new ArrayList<String>(list_data)); // Kopie alle Daten für die Kategorie (Key) speichern
                            list_data.clear(); // Zwischenspeicher leeren
                        }

                        listDataHeader.add(data[1]); // neue Kategorie abspeichern
                    }
                    else
                    {
                        list_data.add(data[0]); // Namen des aktuellen Tools im Zwischenspeicher ablegen
                    }
                }
            }

            listDataChild.put(listDataHeader.get(listDataHeader.size() - 1), list_data); // Kopie alle Daten für die Kategorie (Key) speichern

        } catch (Exception e) {}
    }


    /* Würde in der Navigations-Leiste More->Settings einbauen, was in unserer App aber nicht
     * implementiert wurde..

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_tool, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}
