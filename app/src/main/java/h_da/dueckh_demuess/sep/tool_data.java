package h_da.dueckh_demuess.sep;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;


// zeigt die Details für ein bestimmtes Tool an und gibt die Möglichkeit dieses
// zu bestellen
public class tool_data extends ActionBarActivity
{
    // Kategorie und Tool ID des ausgewählen Tools
    int cat_id = -1;
    int tool_id = -1;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tool_data);

        // Verbindungsobjekte zu allen benötigten Textelementen erstellen
        TextView tool_name = (TextView) findViewById(R.id.tool_name);
        TextView tool_cat = (TextView) findViewById(R.id.tool_cat);
        TextView tool_price = (TextView) findViewById(R.id.tool_price);
        TextView tool_price_dur = (TextView) findViewById(R.id.tool_price_dur);
        TextView tool_desc = (TextView) findViewById(R.id.tool_desc);
        TextView tool_amount = (TextView) findViewById(R.id.tool_amount);

        // übergebene Daten laden und in variablen Speichern
        Intent intent = getIntent();
        cat_id = intent.getIntExtra("cat_id", -1);
        tool_id = intent.getIntExtra("tool_id", -1);

        if(cat_id > -1 && tool_id > -1) // nur wenn auch die benötigten IDs gesetzt wurden
        {
            InputStream input;
            BufferedReader reader;
            String line;

            int cat_counter = -1; // Zähler für die aktuelle Kategorie
            int tool_counter = 0; // Zähler für das aktuelle Tool
            String[] data = null; // enthält die aufgetrennten Daten des Tools
            String cat_name = null; // enthält den Namen der zu letzt verarbeiteten Kategorie

            try
            {
                input = getAssets().open("tools.txt"); // Asset-Datei öffnen, in der alle Tools abgelegt sind
                reader = new BufferedReader(new InputStreamReader(input));

                while ((line = reader.readLine()) != null) // solange Zeilen zum lesen vorhanden sind
                {
                    data = line.split(";"); // Tool-Daten auftrennen

                    if (data.length > 0) // nur wenn auch Daten aufgetrennt werden konnten
                    {
                        if (cat_counter != cat_id) // bis die benötigte Kategorie gefunden wurde
                        {
                            if (data[0].equals("===")) // wenn es bei den Daten um eine Kategorie handelt
                            {
                                cat_counter++; // Counter erhöhen (= ID der aktuellen Kategorie)
                                cat_name = data[1]; // Name der aktuellen Kategorie speichern
                            }
                        }
                        else // es werden gerade die Daten der gesuchten Kategorie verarbeitet
                        {
                            if(tool_counter != tool_id) // wenn es sich nicht um das gesuchte Tool handelt
                            {
                                tool_counter++; // Counter (= Tool-ID) erhöhen
                            }
                            else
                            {
                                break; // Schleife beenden, da das gesuchte Tool gefunden wurde
                            }
                        }
                    }
                }


                if(cat_counter == cat_id && tool_counter == tool_id) // nur wenn das gesuchte Tool auch gefunden wurde
                {
                    // Daten des Tools auf dem View ausgeben
                    tool_name.setText(data[0]);
                    tool_price.setText(data[1] + " €");
                    tool_cat.setText(cat_name);
                    tool_desc.setText(data[3]);

                    switch (data[2]) // anhand der Preis-Dauer switchen
                    {
                        case "h":
                            tool_price_dur.setText("/ Stunde");
                            break;
                        case "d":
                            tool_price_dur.setText("/ Tag");
                            break;
                        case "w":
                            tool_price_dur.setText("/ Woche");
                            break;
                        case "m":
                            tool_price_dur.setText("/ Monat");
                            break;
                    }


                    // der nachfolgende Code zählt wie oft dieses Tool bereits bestellt wurde
                    // und gibt diese Info dann auf dem View aus

                    int amount_counter = 0; // Zähler für die Anzahl der Bestellungen

                    try
                    {
                        InputStream amount_input;
                        BufferedReader amount_reader;
                        String amount_line;

                        amount_input = openFileInput("orders.txt"); // Datei im InternalStorage öffnen
                        amount_reader = new BufferedReader(new InputStreamReader(amount_input));

                        while((amount_line = amount_reader.readLine()) != null) // solange Zeilen zum lesen vorhanden sind
                        {
                            String[] splitter = amount_line.split(";")[0].split(":"); // Order-Data auftrennen

                            if(splitter[0].equals(new Integer(cat_id).toString()) && splitter[1].equals(new Integer(tool_id).toString())) // wenn die Cat-ID & Tool-ID der Bestellung mit denen unseres Tools übereinstimmt
                            {
                                amount_counter++;
                            }
                        }
                    }
                    catch (Exception e) {}

                    tool_amount.setText(new Integer(amount_counter).toString()); // Anzahl der Bestellungen auf dem View ausgeben
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Fehler beim Laden der Daten!", Toast.LENGTH_SHORT).show(); // Fehlermeldung als Bottom-Popup ausgeben
                }
            }
            catch (Exception e) {}
        }
    }


    // wird aufgerufen wenn der Button "Bestellen" angeklickt wird und trägt das aktuelle Tool
    // in die Order-Datei ein
    public void button_orderTool(View view)
    {
        try
        {
            OutputStream output;
            output = openFileOutput("orders.txt", Context.MODE_APPEND); // Datei im InternalStorage öffnen
            output.write((cat_id + ":" + tool_id + ";0\n").getBytes()); // Tool in Datei eintragen (z.B. 1;3;0 -> Kategorie;Gerät;Projekt)
            output.close(); // Datei wieder schließen

            // Anmerkung: Tools werden aktuell immer für Projekt "0" bestellt, da die Verwaltung für
            // mehrere Projekte zwar vorgesehen war, aber aus Zeitgründen nicht mehr implementiert wurde.

            TextView tool_amount = (TextView) findViewById(R.id.tool_amount); // Verbindungsobjekt zur Bestell-Anzahl erstellen
            int amount = Integer.parseInt(tool_amount.getText().toString()); // Anzahl der Bestellungen aus dem Textelement auslesen
            tool_amount.setText(new Integer(++amount).toString()); // Anzahl erhöhen und wieder auf dem View ausgeben

            this.finish(); // diese Activity beenden und somit zur Übersicht aller Geräte zurückspringen
        }
        catch (Exception e) {}
    }


    /* Würde in der Navigations-Leiste More->Settings einbauen, was in unserer App aber nicht
     * implementiert wurde..

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tool_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */
}