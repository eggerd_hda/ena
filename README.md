# Entwicklung nutzerorientierter Anwendungen
*3\. Semester, 2015*  
*Prof. Hans-Peter Wiedling*

Im Rahmen dieses Praktikum haben wir eine Android App entwickelt, über die Handwerker verschiedenste Geräte bestellen können. In der Gestaltung der App waren wir hierbei weitestgehend freigestellt, auch wenn wir die gelernten *Usability-Kriterien* einhalten sollten.

Leider existieren unsere handschriftlichen Unterlagen, wie z.B. Skizzen und Anwendungsszenarien, aus der Planungsphase nicht mehr, weshalb in diesem Repository lediglich die fertige App vorgestellt werden kann.


## Behandelte Themen
- Analyse von Anwendungsszenarien durch benutzerorientiertes Requirements-Engineering
- Usability-Kriterien
    - Effizienz - *wenig Interaktion; Steuerung an Hauptabläufen orientiert*
    - Effektivität - *Können alle Aufgaben vollständig gelöst werden?*
    - Erlernbarkeit - *Kann das UI aus der Benutzererfahrung heraus bedient werden?*
    - Erinnerbarkeit - *Kann man das UI nach einer gewissen Zeit noch sicher bedienen?*
    - Zufriedenheit - *Gestaltung, Feedback, Klarheit*
    - Fehlerbehandlung - *Fehlersituationen durch Bedienung möglichst vermeiden*
- Planung und Dokumentation einer Anwendung
- Entwicklung einer Android App, mit Hilfe von [Android Studio](https://developer.android.com/studio)


## Anforderungen
- Planung der App mit Hilfe von Skizzen und Anwendungsszenarien
- Die Usability-Kriterien sollen eingehalten werden
- Folgende Funktionalität soll verfügbar sein:
    - Geräte sollen bestellt werden können
    - Bestellte Geräte sollen auch wieder abbestellt werden können
    - Geräte sollen neben einem Preis auch eine Kategorie und eine Beschreibung besitzen
    - Es soll eine Übersicht aller bestellten Geräte und deren Kosten geben 
- Geräte sollen einen der folgenden Abrechnungszeiträume besitzen: pro Tag, Stunde, Woche oder Monat
- Der aktuelle Stand (Bestellungen, kalkulierte Kosten etc.) soll beim Schließen der App nicht verloren gehen
- Funktionalität soll getrennt von der GUI entwickelt werden
- Möglichst vollständige und ausführliche Dokumentation im Quellcode
- Die App soll von einer anderen Gruppe getestet werden (und umgekehrt)


## Endergebnis
| Übersicht | Geräteauswahl | Gerätedetails | Bestellte Geräte |
| --- | --- | --- | --- |
| ![Übersicht](https://gitlab.com/eggerd_hda/ena/-/raw/assets/1-overview.png) | ![Geräteauswahl](https://gitlab.com/eggerd_hda/ena/-/raw/assets/2-tools.png) | ![Gerätedetails](https://gitlab.com/eggerd_hda/ena/-/raw/assets/3-details.png) | ![Bestellte Geräte](https://gitlab.com/eggerd_hda/ena/-/raw/assets/4-orders.png) |


## Demo
Die APK zum Installieren der App kann über den nachfolgenden Download-Link heruntergeladen werden. Um diese installieren zu können, muss auf dem Android Gerät jedoch die Option "[Unbekannte Quellen](https://www.droidwiki.org/wiki/Unbekannte_Quellen)" aktiviert sein.

- [APK für Android herunterladen](https://gitlab.com/eggerd_hda/ena/-/raw/assets/ena-app-debug.apk) (1,04 MB)

##### Screen Record
![Video](https://gitlab.com/eggerd_hda/ena/-/raw/assets/demo.mp4)


## Beteiligte Personen
- [Dennis Müßig](https://gitlab.com/demues)
- [Dustin Eckhardt](https://gitlab.com/eggerd)


## Verwendete Software
- [Android Studio](https://developer.android.com/studio) (1.1)